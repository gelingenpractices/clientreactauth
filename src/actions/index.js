import axios from 'axios';
import { browserHistory } from 'react-router';
import { AUTH_USER, UNAUTH_USER, AUTH_ERROR, FETCH_MSG } from './types';

const API_URL = "http://localhost:3090";

export function signinUser({email, password}){
    return (dispatch) => {
        axios.post(`${API_URL}/signin`, { email, password })
            .then(response => {
                //update state to indicate user as authenticated
                dispatch({ type: AUTH_USER });
                //save token in localstorage
                localStorage.setItem('token', response.data.token);
                //navigate to default path of the application
                browserHistory.push('/feature');
            })
            .catch(() => {
                dispatch(authError('Bad login info'));
            });
    };
}

export function signupUser({email, password}){
    return (dispatch) => {
        axios.post(`${API_URL}/signup`, { email, password })
            .then(response => {
                //update state to indicate user as authenticated
                dispatch({ type: AUTH_USER });
                //save token in localstorage
                localStorage.setItem('token', response.data.token);
                //navigate to default path of the application
                browserHistory.push('/feature');
            })
            .catch(response => {
                dispatch(authError(response.response.data.error));
            });
    };
}

export function authError(error){
    return {
        type: AUTH_ERROR,
        payload: error
    };
}

export function signoutUser(){
    localStorage.removeItem('token');

    return {
        type: UNAUTH_USER
    };
}

export function fetchMessage(){
    return dispatch => {
        axios.get(API_URL, {
            headers: { authorization: localStorage.getItem('token') }
        })
        .then(response => {
            dispatch({
                type: FETCH_MSG,
                payload: response.data
            });
        });
    };
}